# qapply

This repository is split into the archive (`pkg`) and the working structure of the current release (`working`).  

Active testing is done on Ubuntu-12.04 and SGE6 on MetrumRG's parallel framework, [Metworx](www.metworx.com).

Like the familiar `lapply` function, `qapply` applies a function to each element of the first argument and returns a list; it does so via grid engine, however, allowing for simple implementation of embarrassingly parallel execution.

In general, you will:

`qapply( <Vector>, FUN, nCores=N)`

## Installation

The easiest way to grab the current version is through `R::devtools`:

`install_bitbucket("metrumrg/qapply",subdir = "working/qapply")`

## User guide

You can find the user guide, discussing the use of this and `parallel::mclapply` [here](https://bitbucket.org/metrumrg/qapply/src/HEAD/wiki/examples.md?at=master)

## Archive

An archive of releases is available [here](https://bitbucket.org/metrumrg/qapply/downloads/).


